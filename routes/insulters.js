const express = require('express');

// Bring each method from insults
const {
    getInsulters,
    getInsulterImage
}
= require('../controllers/insulter');
const router = express.Router();

router.route('/')
.get(
    getInsulters
);

router.route('/image')
.get(
    getInsulterImage
);

module.exports = router;