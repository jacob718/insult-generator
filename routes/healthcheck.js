const express = require('express');

const { healthCheck }
    = require('../controllers/healthcheck');

const router = express.Router();

router.route('/').get(healthCheck);

module.exports = router;