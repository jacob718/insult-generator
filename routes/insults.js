const express = require('express');

// Bring each method from insults
const {
    getInsults, 
    getInsulterImage,
    randomInsult,
    getInsultById,
    createInsult,
    updateInsult,
    deleteInsult
}
    = require('../controllers/insult');
const router = express.Router();

router.route('/')
.get(
    getInsults
)
.post(
    createInsult
);

router.route('/random')
.get(
    randomInsult
);

router.route('/:id')
.get(
    getInsultById
)
.patch(
    updateInsult
)
.delete(
    deleteInsult
);

module.exports = router;