const express = require('express');
const cors = require('cors');
const dotenv = require('dotenv');
const morgan = require('morgan');

// Load env vars
dotenv.config({ path: './config/config.env' });

// Route files
const healthCheck = require('./routes/healthcheck');
const insults = require('./routes/insults');
const insulters = require('./routes/insulters');

// Create server
const app = express();

app.use(cors());

// Body parser: middleware for interpreting JSON elements from requests
app.use(express.json());

// Development logging middleware
if(process.env.NODE_ENV === 'development') {
    app.use(morgan('dev'));
}

// Mount routers to specific url
app.use('/healthcheck', healthCheck);
app.use('/insults', insults);
app.use('/insulters', insulters);

const PORT = process.env.PORT || 5000;
var HOST = process.env.YOUR_HOST || '0.0.0.0';

const server = app.listen(PORT, HOST, console.log(`Server running in ${process.env.NODE_ENV} mode on port ${PORT}`));