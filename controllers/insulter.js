const asyncHandler = require('../middleware/async');
const minifyRecord = require('../utils/minifyRecord');
const getRandomInt = require('../utils/getRandomInt');
var Airtable = require('airtable');
const Query = require('airtable/lib/query');
var base = new Airtable({apiKey: process.env.AIRTABLE_KEY}).base(process.env.AIRTABLE_BASE_ID);

const table = base('insulters')

// @desc    Get array of insulters
// @route   GET /api/v1/insulters
// @access  Private
exports.getInsulters = asyncHandler(async (req, res, next) => {

    const records = await table.select().all();

    // Create array of all unique insulters
    var insulters = [];
    var insulter;

    for(i = 0; i < records.length; i++) {
        insulter = records[i].fields.Insulter;

        insulters.push(insulter);

    };

    res.status(200).json({
        success: true,
        data: insulters
    });
});

// @desc    Get insulter image
// @route   GET /api/v1/insulters/image?insulter
exports.getInsulterImage = asyncHandler(async (req, res, next) => {
    var records;

    records = await table.select({
        filterByFormula: `{Insulter} = "${req.query.insulter}"`
    }).all();

    var data = null;
    if (records) {
        data = records[0]._rawJson.fields.Picture[0].url;
    }
    res.status(200).json({
        success: true,
        data: data,
    });
})