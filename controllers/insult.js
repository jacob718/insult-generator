const asyncHandler = require('../middleware/async');
const minifyRecord = require('../utils/minifyRecord');
const getRandomInt = require('../utils/getRandomInt');
var Airtable = require('airtable');
const Query = require('airtable/lib/query');
var base = new Airtable({apiKey: process.env.AIRTABLE_KEY}).base(process.env.AIRTABLE_BASE_ID);

const table = base('insults')

// @desc    Get all insults
// @route   GET /api/v1/insult
// @access  Private
exports.getInsults = asyncHandler(async (req, res, next) => {

    const records = await table.select().all();

    for(i = 0; i < records.length; i++) {
        records[i] = minifyRecord(records[i])
    };

    res.status(200).json({
        success: true,
        data: records
    });
});

// @desc    Get a random insult
// @route   GET /api/v1/insults/random
// @access  Private
exports.randomInsult = asyncHandler(async (req, res, next) => {
    
    var records;

    if(req.query.insulter) {
        records = await table.select({
            filterByFormula: `{Insulter} = "${req.query.insulter}"`
        }).all();
    }
    else {
        records = await table.select().all();
    }

    for(i = 0; i < records.length; i++) {
        records[i] = minifyRecord(records[i])
    };

    const rand = getRandomInt(records.length);

    var data = records[rand];
    if(records.length == 0) {
        data = {};
    }

    res.status(200).json({
        success: true,
        data: data,
    });
});

// @desc Get insult by id
// @route GET /api/v1/insult/:id
// @access Public
exports.getInsultById = asyncHandler(async (req, res, next) => {

    var record = await table.find(req.params.id);

    record = minifyRecord(record);

    res.status(200).json({
        success: true,
        data: record
    });
});

// @desc    Create a new insult
// @route   POST /api/v1/insult
// @access  Private
exports.createInsult = asyncHandler(async (req, res, next) => {

    var record = await table.create(req.body);

    record = minifyRecord(record);

    res.status(200).json({
        success: true,
        data: record
    });
});

// @desc Update an insult
// @route PATCH /api/v1/insult/:id
// @access Private
exports.updateInsult = asyncHandler(async (req, res, next) => {
    var record = await table.update(req.params.id, req.body);
    record = minifyRecord(record);

    res.status(200).json({
        success: true,
        data: record
    });
});

// @desc    Delete an insult
// @route   DELETE /api/v1/insult/:id
// @access  Private
exports.deleteInsult = asyncHandler(async (req, res, next) => {
    await table.destroy(req.params.id);

    res.status(200).json({
        success: true,
        data: {}
    });
});